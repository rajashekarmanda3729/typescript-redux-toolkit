import React, { useEffect, useRef } from "react"
import TodoItem from "./components/TodoItem"
import { useAppDispatch, useAppSelector } from "./redux/hooks"
import { onAddTodo, onChangeName } from "./redux/todoSlice"

function App() {

  const inputElement = useRef<HTMLInputElement>(null)

  useEffect(() => {
    inputElement.current?.focus()
  }, [])

  const { todoName, todosList } = useAppSelector(store => store.todo)
  const dispatch = useAppDispatch()

  const onSubmitTodo = (event: React.MouseEvent<HTMLFormElement>) => {
    event.preventDefault()
    dispatch(onAddTodo())
    inputElement.current?.focus()
  }

  return (
    <main className='container-fluid m-3 mt-5'>

      <div className="row m-2">

        <h1 className="text-center">Todo App</h1>
        <form className="d-flex col-12 justify-content-center" onSubmit={onSubmitTodo}>
          <input ref={inputElement} placeholder="enter todo here" type="text" className="me-1 p-2 rounded rounded-4 w-25 fs-5" value={todoName} onChange={(event) => dispatch(onChangeName(event.target.value))} />
          <button className="btn btn-primary" type="submit" >Add Todo</button>
        </form>

        <section className="row">
          <div className="col-12 mt-3 d-flex flex-column justify-content-center align-items-center">
            <div className="d-flex justify-content-evenly w-75">
              <button className="btn btn-secondary">All</button>
              <button className="btn btn-light">Active</button>
              <button className="btn btn-light">Pending</button>
            </div>
            <hr className="w-50 border border-2 border-dark" />
          </div>
        </section>

      </div>

      <section className="d-flex flex-column justify-content-center align-items-center">
        {
          todosList.length !== 0 ? todosList.map(eachTodo => {
            return <TodoItem key={eachTodo} todo={eachTodo} />
          }) :
            <div className="d-flex w-50 justify-content-center align-items-center mt-5">
              <h1 className="text-secondary text-opacity-25">No Todo's to show</h1>
            </div>
        }
      </section>

    </main >
  )
}

export default App
