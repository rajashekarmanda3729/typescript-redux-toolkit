import { createSlice } from "@reduxjs/toolkit";
import { PayloadAction } from "@reduxjs/toolkit";

type InitialState = {
    todosList: string[],
    todoName: string,
}

const initialState: InitialState = {
    todosList: [],
    todoName: ''
}

export const todoSlice = createSlice({
    name: 'todo',
    initialState,
    reducers: {
        onChangeName: (state, action: PayloadAction<string>) => {
            state.todoName = action.payload
        },
        onAddTodo: (state) => {
            state.todosList.push(state.todoName)
            state.todoName = ''
        },
        onDeleteTodo: (state, action: PayloadAction<string>) => {
            const filterArr: string[] = state.todosList.filter(each => each !== action.payload)
            state.todosList = filterArr
        }
    }
})

export const { onChangeName, onAddTodo, onDeleteTodo } = todoSlice.actions

export default todoSlice.reducer