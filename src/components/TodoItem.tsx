import { MdDelete } from 'react-icons/md'
import { useAppDispatch } from '../redux/hooks'
import { onDeleteTodo } from '../redux/todoSlice'

type TodoProps = {
    key: string,
    todo: string
}

const TodoItem = (props: TodoProps) => {

    const dispatch = useAppDispatch()

    return (
        <div className="d-flex w-50 m-1 shadow ps-2 mt-3 p-1 rounded rounded-3">
            <div className="col-12 d-flex justify-content-between align-items-center p-1">
                <div className="">
                    <input type="checkbox" className="me-2" id="title" />
                    <label htmlFor="title" className="fs-4">{props.todo}</label>
                </div>
                <button className="btn" onClick={() => dispatch(onDeleteTodo(props.todo))}>
                    <MdDelete size={20} />
                </button>
            </div>
        </div>
    )
}

export default TodoItem